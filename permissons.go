package permissons

import (
	"strconv"
)

var (
	charsToNum = map[rune]int64{
		'r': 4,
		'w': 2,
		'x': 1,
		'-': 0,
	}
	numToChars = map[rune]string{
		'7': "rwx",
		'6': "rw-",
		'5': "r-x",
		'4': "r--",
		'3': "-wx",
		'2': "-w-",
		'1': "--x",
		'0': "---",
	}
)

func CharsToNum(p string) string {
	res := ""
	if len(p) == 0 {
		return res
	}
	if len(p)%3 != 0 {
		l := len(p) - 1
		if l%3 != 0 {
			return res
		}
		p = p[1:]
	}
	subT := int64(0)
	for i, r := range p {
		subT += charsToNum[r]
		if i != 0 && (i+1)%3 == 0 {
			res += strconv.FormatInt(subT, 16)
			subT = 0
		}
	}
	return res
}

func NumToChars(p string) string {
	acu := ""
	for _, r := range p {
		v, ok := numToChars[r]
		if !ok {
			return ""
		}
		acu += v
	}
	return acu
}
