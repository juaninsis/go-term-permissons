package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	permissons "gitlab.com/juaninsis/go-term-permissons"
)

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) < 1 {
		return
	}
	arg := args[0]
	if strings.Contains(arg, "/") {
		info, err := os.Stat(arg)
		if err != nil {
			fmt.Println(err)
			return
		}
		m := info.Mode()
		np := permissons.CharsToNum(m.String())
		cp := permissons.NumToChars(np)
		fmt.Println(cp)
		fmt.Println(np)
		return
	}
	_, err := strconv.ParseInt(arg, 10, 64)
	if err != nil {
		fmt.Println(permissons.CharsToNum(arg))
		return
	}
	fmt.Println(permissons.NumToChars(arg))
}
