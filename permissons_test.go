package permissons

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCharsToNum(t *testing.T) {
	tests := []struct {
		name, input, output string
	}{
		{
			name:   "r-- successful chars_to_num case",
			input:  "r--",
			output: "4",
		},
		{
			name:   "-w- successful chars_to_num case",
			input:  "-w-",
			output: "2",
		},
		{
			name:   "--x successful chars_to_num case",
			input:  "--x",
			output: "1",
		},
		{
			name:   "-wx successful chars_to_num case",
			input:  "-wx",
			output: "3",
		},
		{
			name:   "rwx successful chars_to_num case",
			input:  "rwx",
			output: "7",
		},
		{
			name:   "--- successful chars_to_num case",
			input:  "---",
			output: "0",
		},
		{
			name:   "rwxr--rwx successful chars_to_num case",
			input:  "rwxr--rwx",
			output: "747",
		},
	}

	for _, test := range tests {
		res := CharsToNum(test.input)
		assert.Equal(t, test.output, res, test.name)
	}
}

func TestNumToChars(t *testing.T) {
	tests := []struct {
		name, input, output string
	}{
		{
			name:   "4 successful chars_to_num case",
			input:  "4",
			output: "r--",
		},
		{
			name:   "2 successful chars_to_num case",
			input:  "2",
			output: "-w-",
		},
		{
			name:   "1 successful chars_to_num case",
			input:  "1",
			output: "--x",
		},
		{
			name:   "3 successful chars_to_num case",
			input:  "3",
			output: "-wx",
		},
		{
			name:   "7 successful chars_to_num case",
			input:  "7",
			output: "rwx",
		},
		{
			name:   "0 successful chars_to_num case",
			input:  "0",
			output: "---",
		},
		{
			name:   "724 successful chars_to_num case",
			input:  "724",
			output: "rwx-w-r--",
		},
	}

	for _, test := range tests {
		res := NumToChars(test.input)
		assert.Equal(t, test.output, res, test.name)
	}
}
